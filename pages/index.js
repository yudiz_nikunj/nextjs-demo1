import Link from 'next/link';
import TimeAgo from 'react-timeago'


function blog( { results }){
  return( 
    <div class="common-news-list ng-star-inserted">
      {results.map((post) => (
        
        <div  class="common-news-item d-flex common-shadow ng-star-inserted">
           <div  class="common-news-img common-zoom-container fullblock-link-container" tabindex="0">
              <div  class="common-zoom-img ng-lazyloaded" style={{  backgroundImage: `url("`+post.sImage+`")` }}></div>
              <Link  class="fullblock-link" title="pro-kabaddi-warriors-vs-bulls" href={ post.sSlug }><a></a></Link>
           </div>
           <div  class="common-news-desc">
              <div  class="news-excerpt">
                 <h5 ><Link  title="pro-kabaddi-warriors-vs-bulls" href={ post.sSlug }><a>{ post.sTitle}</a></Link></h5>
                 <p  tabindex="0"> { post.sDescription}</p>
              </div>
              <div  class="author-info d-flex justify-content-between align-items-center">
                 <p  class="author"><a  title="Sports-Info" href="#">Sports Info</a> <TimeAgo date={post.dCreatedAt}  /> </p>
                 <ul  class="count-list">
                    <li >
                       <p  class="comment"><span >{ post.nCommentsCount }</span></p>
                    </li>
                    <li >
                       <p  class="view"><span >{ post.nViewCounts }</span></p>
                    </li>
                 </ul>
              </div>
           </div>
        </div>
      ))}
      <div class="text-center"><a class="theme-btn down-btn ng-star-inserted"> Load More<span></span></a></div>
    </div>
    );
}

export async function getStaticProps (){

  const formData = new URLSearchParams();
  formData.append('nStart', 0);
  formData.append('nLimit', 10);
  formData.append('eSort', 'Latest');
  formData.append('bRemoveBannerPosts', true);

  const res = await fetch ('https://backend.sports.info/api/v1/posts/recent',{
    method : 'POST',
    body: formData,
  });
  const posts = await res.json();
  const results = posts.data;

  return {
    props : {
      results,
    }
  }
 
}

export default blog