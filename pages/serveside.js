function Post({ post }) {
  return (
      <h1>{  post.title }</h1>
   );
}



// This also gets called at build time
export async function getServerSideProps({ params }) {
  
  // params contains the post `id`.
  // If the route is like /posts/1, then params.id is 1
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/1`)
  const post = await res.json()

  // Pass post data to the page via props
  return { props: { post } }
}

export default Post