import TimeAgo from 'react-timeago'
function BlogDetail({ postdetail }) {
  
  return (
    <div id="blogOuterSection" class="ng-star-inserted">
      <section class="blog-banner ng-lazyloaded" style={{  backgroundImage: `url("`+postdetail.sImage+`")` }} >
        
        <div class="banner-inner">
          <div class="container">
            <div class="banner-desc">
              <h1 class="white-text">{ postdetail.sTitle }</h1>
              <div class="author-info d-flex justify-content-between align-items-center">
                <p class="author"><a href="#">Sports Info</a> <TimeAgo date={postdetail.dCreatedAt}  /></p>
                <ul class="count-list">
                  <li>
                    <p class="comment white-icon"><span>{ postdetail.nCommentsCount }</span>
                    </p>
                  </li>
                  <li>
                    <p class="view white-icon"><span>{ postdetail.nViewCounts }</span>
                    </p>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="common-section blog-content-section">
        <div class="container">
          <div class="row sidebar-row">
            <div class="content-col">
              <div class="blog-inner d-flex">
                
                <div class="blog-content-block">
                  <div className="blog-content" dangerouslySetInnerHTML={{ __html:  postdetail.sDescription  }}>

                  </div>              
                  
                  <div class="source-text d-flex justify-content-end">                    
                    <p class="ng-star-inserted">Source: The source of this content is our cricket news platform <a href="https://www.crictracker.com">Crictracker.</a>
                    </p>
                  </div>
                  <div class="blog-author d-flex align-items-center">
                    <div class="author-img ng-lazyloaded" style={{  backgroundImage: `url("https://media.sports.info/560x315/profilepicture/5cefd346b2abd24197216447_1559738153292_favicon_512x512.png")` }} ></div>
                    <div class="author-desc">
                      <h6><a href="#">Sports Info</a></h6>
                      <p>SportsInfo offers cricket, soccer, kabaddi, tennis, badminton, racing, basketball and other sports news, articles, videos, live coverage &amp; live scores, player rankings &amp; team rankings. Also, offers minute details of any match along with live commentary.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="sidebar-col ng-star-inserted">
              <div class="sticky-sidebar">
                <app-custom-ads-widget _nghost-c11="">
                </app-custom-ads-widget>
                <div class="common-stories-widget common-shadow rounded-box">                  
                </div>
                <app-custom-ads-widget _nghost-c11="">  
                </app-custom-ads-widget>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}
//BlogDetail

export async function getStaticPaths() {
  const res = await fetch('https://backend.sports.info/api/v1/posts/recent')
  const posts = await res.json()
  const results = posts.data;  
  const paths = results.map((post) => ({
    params: { id: post.sSlug },
  }));
  return { paths, fallback: false }
}


export async function getStaticProps({ params }) { 
  const res = await fetch(`https://backend.sports.info/api/v1/posts/view/${params.id}`)
  const post = await res.json()
  const postdetail = post.data;
  return { props: { postdetail } }
}

export default BlogDetail