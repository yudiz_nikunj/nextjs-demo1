import '../styles/globals.css'
import '../styles/bootstrap.css'
import '../styles/main.css'


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
